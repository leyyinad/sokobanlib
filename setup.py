from setuptools import setup, find_packages

with open('README.adoc') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name='sokobanlib',
    version='0.1.0',
    description='Sokoban toolbelt',
    long_description=readme,
    author='D. Haus',
    author_email='d@danielhaus.de',
    url='https://gitlab.com/leyyinad/sokobanlib',
    license=license,
    install_requires=required,
    packages=find_packages(
        exclude=(
            'tests',
            'docs')),
    entry_points={
        'console_scripts': [
            'sokoban-util = sokobanlib.util.__main__:main',
        ]
    }
)
