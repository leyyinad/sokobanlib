from sokobanlib.model.move import Move
from sokobanlib.solver import Solver, solve
from sokobanlib.parser import parse, parse_solution


def test_pos_after_move():
    s = Solver()

    assert s.pos_after_move((5, 5), Move.LEFT) == (4, 5)
    assert s.pos_after_move((5, 5), Move.RIGHT) == (6, 5)
    assert s.pos_after_move((5, 5), Move.UP) == (5, 4)
    assert s.pos_after_move((5, 5), Move.DOWN) == (5, 6)

    assert s.pos_after_move((5, 5), Move.PUSH_LEFT) == (4, 5)
    assert s.pos_after_move((5, 5), Move.PUSH_RIGHT) == (6, 5)
    assert s.pos_after_move((5, 5), Move.PUSH_UP) == (5, 4)
    assert s.pos_after_move((5, 5), Move.PUSH_DOWN) == (5, 6)


def test_solver_move_right1():
    s0 = parse("""#####
                  #   #
                  # @ #
                  #   #
                  #####""")

    s1 = parse("""#####
                  #   #
                  #  @#
                  #   #
                  #####""")

    Solver().move(s0, Move.RIGHT)
    assert s0 == s1


def test_solver_push_right1():
    s0 = parse("""#####
                  #   #
                  #@$ #
                  #   #
                  #####""")

    s1 = parse("""#####
                  #   #
                  # @$#
                  #   #
                  #####""")

    Solver().push(s0, Move.PUSH_RIGHT)
    assert s0 == s1


def test_solver_push_right2():
    s0 = parse("""#####
                  #@$.#
                  #####""")

    s1 = parse("""#####
                  # @*#
                  #####""")

    Solver().push(s0, Move.PUSH_RIGHT)
    assert s0 == s1


def test_solver_push_up():
    s0 = parse("""#####
                  # . #
                  # $ #
                  # @ #
                  #####""")

    s1 = parse("""#####
                  # * #
                  # @ #
                  #   #
                  #####""")

    Solver().push(s0, Move.PUSH_UP)
    assert s0 == s1


def test_solver_push_right():
    s0 = parse('#@$.#')
    s1 = parse('# @*#')

    Solver().push(s0, Move.PUSH_RIGHT)
    assert s0 == s1


def test_solver_generate1():
    lvl = """
        $####
        #   #
        # @ #
        #   #
        #####
     """

    assert len(Solver().generate(parse(lvl))) == 9


def test_solver_generate2():
    lvl = """
        ###
        # ###
        # @ #
        # $.#
        #####
     """

    assert len(Solver().generate(parse(lvl))) == 13


def test_solver_generate3():
    lvl = """
        #####
        #   #####
        # . $   #
        #  @    #
        #########
    """

    assert len(Solver().generate(parse(lvl))) == 209


def test_solver_solve1():
    lvl = """
        #####
        #   #####
        # . $   #
        #  @    #
        #########
    """

    assert parse_solution('rruLL') in solve(parse(lvl))


def test_solver_solve2():
    lvl = '# $@.#'
    assert not solve(parse(lvl))
