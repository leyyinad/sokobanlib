from sokobanlib.model.state import SokobanState
from sokobanlib.model.tile import Tile
from sokobanlib.parser import parse


def test_hashes_equal():
    s0 = SokobanState()
    s1 = SokobanState()

    s0.tiles = [[Tile.GOAL, Tile.FLOOR]]
    s1.tiles = [[Tile.GOAL, Tile.FLOOR]]

    assert s0 == s1


def test_hashes_differ():
    s0 = SokobanState()
    s1 = SokobanState()

    s0.tiles = [[Tile.FLOOR, Tile.GOAL]]
    s1.tiles = [[Tile.GOAL, Tile.FLOOR]]

    assert s0 != s1


def test_get_pos():
    assert parse('# @ #').pos == (2, 0)


def test_set_pos():
    s0 = parse('# @ #')
    s1 = parse('#  @#')

    s0.pos = 3, 0
    assert s0 == s1
