from sokobanlib.model.tile import Tile


def test_wall():
    assert Tile.WALL.is_wall
    assert not Tile.WALL.is_walkable


def test_player():
    assert Tile.PLAYER.is_player
    assert Tile.PLAYER_ON_GOAL.is_player
    assert Tile.PLAYER.remove_player() is Tile.FLOOR
    assert Tile.PLAYER_ON_GOAL.remove_player() is Tile.GOAL


def test_box():
    assert Tile.BOX.is_box
    assert Tile.BOX_ON_GOAL.is_box
    assert Tile.BOX.remove_box() is Tile.FLOOR
    assert Tile.BOX_ON_GOAL.remove_box() is Tile.GOAL
