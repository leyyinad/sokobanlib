from sokobanlib.model.move import Move


def test_move_directions():
    assert Move.UP.is_up
    assert Move.DOWN.is_down
    assert Move.LEFT.is_left
    assert Move.RIGHT.is_right

    assert not Move.UP.is_down
    assert not Move.DOWN.is_left
    assert not Move.LEFT.is_up
    assert not Move.RIGHT.is_down


def test_push_directions():
    assert Move.PUSH_UP.is_up
    assert Move.PUSH_DOWN.is_down
    assert Move.PUSH_LEFT.is_left
    assert Move.PUSH_RIGHT.is_right

    assert not Move.PUSH_UP.is_down


def test_offsets():
    assert Move.UP.offset_x is 0
    assert Move.UP.offset_y is -1
