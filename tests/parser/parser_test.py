from sokobanlib.model.tile import Tile
from sokobanlib.parser import Parser

WLL, PLR, PLG, BOX, BXG, GOL, FLR = [
    t for t in Tile]


def test_parser():
    level = """
        Minimal
        #####
        #@$.#
        #####
        Title: Minimal
        Author: unknown
    """

    s = Parser().parse(level)

    assert s.title == 'Minimal'
    assert s.caption == 'Minimal'
    assert s.author == 'unknown'

    assert len(s.tiles) == 3

    assert s.tiles == [
        [WLL, WLL, WLL, WLL, WLL],
        [WLL, PLR, BOX, GOL, WLL],
        [WLL, WLL, WLL, WLL, WLL],
    ]

    assert s.pos == (1, 1)
    assert s.boxes == {(2, 1)}
    assert s.goals == {(3, 1)}
