import typing
from sokobanlib.model.move import Move
from sokobanlib.model.solution import Solution
from sokobanlib.model.tile import Tile

if typing.TYPE_CHECKING:
    from sokobanlib.model.state import SokobanState


def repr_state(state: 'SokobanState'):
    lines = []
    for row in state.tiles:
        line = ''
        for col in row:
            line += get_tile_char(col)
        lines.append(line)
    return "\n".join(lines)


def repr_solution(solution: Solution) -> str:
    return ''.join(get_move_char(c) for c in solution)


def get_tile_char(tile: Tile, minimized: bool = False) -> str:
    if tile == Tile.WALL:
        return '#'
    elif tile == Tile.PLAYER_ON_GOAL:
        return '+'
    elif tile == Tile.PLAYER:
        return '@'
    elif tile == Tile.BOX:
        return '$'
    elif tile == Tile.BOX_ON_GOAL:
        return '*'
    elif tile == Tile.GOAL:
        return '.'
    elif tile == Tile.FLOOR:
        return '-' if minimized else ' '
    else:
        return '?'


def get_move_char(move: Move) -> str:
    if move == Move.UP:
        return 'u'
    elif move == Move.DOWN:
        return 'd'
    elif move == Move.LEFT:
        return 'l'
    elif move == Move.RIGHT:
        return 'r'
    elif move == Move.PUSH_UP:
        return 'U'
    elif move == Move.PUSH_DOWN:
        return 'D'
    elif move == Move.PUSH_LEFT:
        return 'L'
    elif move == Move.PUSH_RIGHT:
        return 'R'
    else:
        return '?'
