import re
from typing import List

from sokobanlib.model.move import Move
from sokobanlib.model.state import SokobanState
from sokobanlib.model.tile import Tile
from sokobanlib.solver import Solution


def parse(source: str) -> SokobanState:
    return Parser().parse(source)


def parse_solution(s: str) -> Solution:
    return [parse_move(c) for c in s]


class Parser:
    def parse(self, source: str) -> SokobanState:
        s = SokobanState()

        tiles: List[List[Tile]] = []

        for i, line in enumerate(source.strip().splitlines()):
            line = line.strip()

            if i == 0 and re.match(r'[a-z]+', line, re.I):
                s.caption = line
                continue

            if not line:
                continue

            if ':' in line:
                key, value = [s.strip() for s in line.split(':')]
                key = key.lower()

                if key == 'title':
                    s.title = value
                elif key == 'author':
                    s.author = value
                continue

            for tile in self.parse_line(line):
                tiles.append(tile)

        s.tiles = tiles

        return s

    def parse_line(self, line: str) -> List[List[Tile]]:
        result = []
        current_line: List[Tile] = []

        for i in range(len(line)):
            c = line[i]
            if c == '|':
                result.append(current_line)
                current_line = []
                continue
            ms = re.match(r'^\d+', line[i:])
            if ms:
                n = int(ms[0])
                i += len(ms[0])
                t = parse_tile(line[i])
                current_line += [t for j in range(n - 1)]
                continue

            current_line.append(parse_tile(c))
        result.append(current_line)
        return result


def parse_tile(char: str) -> Tile:
    if char == '#':
        return Tile.WALL
    elif char == '@':
        return Tile.PLAYER
    elif char == '+':
        return Tile.PLAYER_ON_GOAL
    elif char == '$':
        return Tile.BOX
    elif char == '*':
        return Tile.BOX_ON_GOAL
    elif char == '.':
        return Tile.GOAL
    elif char in (' ', '-', '_'):
        return Tile.FLOOR
    else:
        raise Exception(f"Unexpected char '{char}'")


def parse_move(char: str) -> Move:
    if char == 'u':
        return Move.UP
    elif char == 'd':
        return Move.DOWN
    elif char == 'l':
        return Move.LEFT
    elif char == 'r':
        return Move.RIGHT
    elif char == 'U':
        return Move.PUSH_UP
    elif char == 'D':
        return Move.PUSH_DOWN
    elif char == 'L':
        return Move.PUSH_LEFT
    elif char == 'R':
        return Move.PUSH_RIGHT
    else:
        raise Exception(f"Unexpected char '{char}'")
