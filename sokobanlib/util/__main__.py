#!/usr/bin/env python3
import argparse
import sys
from typing import Optional, TextIO

from sokobanlib.minimize import minimize
from sokobanlib.model.state import SokobanState
from sokobanlib.parser import Parser, parse_solution
from sokobanlib.print import repr_solution, repr_state
from sokobanlib.solver import Solver


class UtilOptions(argparse.Namespace):
    verbosity: int
    quietness: int
    minimize: bool
    solve: bool
    run: str
    infile: TextIO
    outfile: TextIO


def main():
    run(parse_args())


def run(options: UtilOptions):
    state: Optional[SokobanState] = None

    state = load(options.infile)
    if options.minimize:
        run_minimze(state, options)
    elif options.solve:
        run_solve(state, options)
    elif options.run:
        run_simulate(state, options)
    else:
        run_print(state, options)


def run_print(state: SokobanState, options: UtilOptions):
    print(repr_state(state), file=options.outfile)


def run_minimze(state: SokobanState, options: UtilOptions):
    print(minimize(state), file=options.outfile)


def run_solve(state: SokobanState, options: UtilOptions):
    solutions = Solver().solve(state)
    if solutions:
        print(f"{repr_solution(solutions[0])}", file=options.outfile)
    else:
        print("No solutions.", file=options.outfile)


def run_simulate(state: SokobanState, options: UtilOptions):
    try:
        solution = parse_solution(options.run[0])
        state = Solver().simulate(state, solution)
    except Exception as e:
        print(e, file=options.outfile)
        sys.exit(1)

    if state.is_solved():
        print('Solved.', file=options.outfile)
    else:
        print('Not solved.', file=options.outfile)


def load(f: TextIO) -> SokobanState:
    return Parser().parse(f.read())


def parse_args():
    parser = argparse.ArgumentParser(prog='sokoban-util',
                                     description='Sokobanlib CLI util.')

    parser.add_argument('-m', '--minimize',
                        help='Minimize', action='store_true')
    parser.add_argument('-s', '--solve', help='Solve', action='store_true')
    parser.add_argument('-r', '--run', nargs=1, help='Run')

    parser.add_argument('infile', nargs='?', type=argparse.FileType('r'),
                        default=sys.stdin, help='Input file')
    parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'),
                        default=sys.stdout, help='Output file')

    return parser.parse_args(namespace=UtilOptions())


if __name__ == "__main__":
    main()
