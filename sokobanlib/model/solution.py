from typing import List
from sokobanlib.model.move import Move

Solution = List[Move]
