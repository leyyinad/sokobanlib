from enum import Enum, auto


class Move(Enum):
    UP = auto()
    DOWN = auto()
    LEFT = auto()
    RIGHT = auto()
    PUSH_UP = auto()
    PUSH_DOWN = auto()
    PUSH_LEFT = auto()
    PUSH_RIGHT = auto()

    @property
    def is_up(self) -> bool:
        return self in (Move.UP, Move.PUSH_UP)

    @property
    def is_down(self) -> bool:
        return self in (Move.DOWN, Move.PUSH_DOWN)

    @property
    def is_left(self) -> bool:
        return self in (Move.LEFT, Move.PUSH_LEFT)

    @property
    def is_right(self) -> bool:
        return self in (Move.RIGHT, Move.PUSH_RIGHT)

    @property
    def is_push(self) -> bool:
        return self in (
            Move.PUSH_UP,
            Move.PUSH_DOWN,
            Move.PUSH_LEFT,
            Move.PUSH_RIGHT)

    @property
    def offset_x(self) -> int:
        if self.is_left:
            return -1
        elif self.is_right:
            return 1
        else:
            return 0

    @property
    def offset_y(self) -> int:
        if self.is_up:
            return -1
        elif self.is_down:
            return 1
        else:
            return 0
