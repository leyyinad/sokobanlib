from enum import Enum, auto


class Tile(Enum):
    WALL = auto()
    PLAYER = auto()
    PLAYER_ON_GOAL = auto()
    BOX = auto()
    BOX_ON_GOAL = auto()
    GOAL = auto()
    FLOOR = auto()

    @property
    def is_wall(self):
        return self is Tile.WALL

    @property
    def is_player(self):
        return self in (Tile.PLAYER, Tile.PLAYER_ON_GOAL)

    @property
    def is_box(self):
        return self in (Tile.BOX, Tile.BOX_ON_GOAL)

    @property
    def is_goal(self):
        return self in (Tile.GOAL, Tile.BOX_ON_GOAL, Tile.PLAYER_ON_GOAL)

    @property
    def is_walkable(self):
        return self in (Tile.FLOOR, Tile.GOAL)

    def add_box(self):
        if self.is_box:
            return self
        else:
            return Tile.BOX_ON_GOAL if self.is_goal else Tile.BOX

    def remove_box(self):
        if not self.is_box:
            return self
        else:
            return Tile.GOAL if self.is_goal else Tile.FLOOR

    def add_player(self):
        if self.is_player:
            return self
        else:
            return Tile.PLAYER_ON_GOAL if self.is_goal else Tile.PLAYER

    def remove_player(self):
        if not self.is_player:
            return self
        else:
            return Tile.GOAL if self.is_goal else Tile.FLOOR
