from typing import Iterable, List, Optional, Set, Tuple

from sokobanlib.model.tile import Tile
from sokobanlib.print import repr_state

Pos = Tuple[int, int]


class SokobanState:
    title: Optional[str] = None
    caption: Optional[str] = None
    author: Optional[str] = None

    _tiles: List[Tile]
    _size: Pos

    _player: Pos
    _boxes: Set[Pos]
    _goals: Set[Pos]

    def __init__(self):
        self._size = 0, 0
        self._tiles = []

        self._player = 0, 0
        self._boxes = set()
        self._goals = set()

    def __hash__(self) -> int:
        return hash((self._size, tuple(self._tiles)))

    def __eq__(self, o: object) -> bool:
        return isinstance(o, SokobanState) and hash(self) == hash(o)

    def __repr__(self) -> str:
        return repr_state(self)

    def __getitem__(self, pos: Pos) -> Tile:
        try:
            return self.tiles[pos[1]][pos[0]]
        except IndexError:
            return Tile.WALL

    def __setitem__(self, pos: Pos, tile: Tile):
        x, y = pos
        i = x + y * self._size[0]

        old_tile = self._tiles[i]

        if old_tile.is_player:
            self._player = 0, 0
        if old_tile.is_box:
            self._boxes.remove(pos)
        if old_tile.is_goal:
            self._goals.remove(pos)

        self._tiles[i] = tile

        if tile.is_player:
            self._player = pos
        if tile.is_box:
            self._boxes.add(pos)
        if tile.is_goal:
            self._goals.add(pos)

    def find_boxes(self) -> Iterable[Pos]:
        for y, row in enumerate(self.tiles):
            for x, col in enumerate(row):
                if col.is_box:
                    yield x, y

    def is_solved(self) -> bool:
        for row in self.tiles:
            if Tile.BOX in row:
                return False
        return True

    @property
    def tiles(self) -> List[List[Tile]]:
        cols, rows = self._size
        tiles: List[List[Tile]] = []
        for y in range(rows):
            offset = cols * y
            tiles += [self._tiles[offset:offset + cols]]
        return tiles

    @tiles.setter
    def tiles(self, rows: List[List[Tile]]) -> None:
        w = max(len(cols) for cols in rows)
        h = len(rows)
        self._size = w, h

        tiles = [Tile.FLOOR] * self._size[0] * self._size[1]
        boxes: Set[Pos] = set()
        goals: Set[Pos] = set()
        for y in range(h):
            for x in range(len(rows[y])):
                tile = rows[y][x]
                tiles[y * w + x] = tile
                pos = x, y
                if tile.is_player:
                    self._player = pos
                elif tile.is_box:
                    boxes.add(pos)
                if tile.is_goal:
                    goals.add(pos)

        self._tiles = tiles
        self._boxes = boxes
        self._goals = goals

    def find_player(self) -> Pos:
        for y, row in enumerate(self.tiles):
            for x, tile in enumerate(row):
                if tile.is_player:
                    return x, y
        return 0, 0

    @property
    def pos(self) -> Pos:
        return self._player

    @pos.setter
    def pos(self, pos: Pos) -> None:
        self[self._player] = self[self._player].remove_player()
        self._player = pos
        self[pos] = self[pos].add_player()

    @property
    def boxes(self) -> Set[Pos]:
        return self._boxes

    @property
    def goals(self) -> Set[Pos]:
        return self._goals
