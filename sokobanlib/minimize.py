from sokobanlib.model.state import SokobanState
from sokobanlib.model.tile import Tile
from sokobanlib.print import get_tile_char


def minimize(state: SokobanState) -> str:
    result = ''
    for i, row in enumerate(state.tiles):
        if i > 0:
            result += '|'

        n, t = 0, None
        for j in range(len(row)):
            col = row[j]

            if col == t:
                n += 1
            else:
                if n > 0 and t:
                    result += rle(n, t)
                    t = None
                t, n = col, 1

        if n > 0 and t:
            result += rle(n, t)
            t = None

    return result


def rle(n: int, t: Tile) -> str:
    if n == 0:
        return ''

    c = get_tile_char(t, True)

    if n == 1:
        return c
    elif n == 2:
        return f"{c}{c}"
    else:
        return f"{n}{c}"
