from copy import deepcopy
from typing import Dict, Iterable, List, Optional

from sokobanlib.model.move import Move
from sokobanlib.model.solution import Solution
from sokobanlib.model.state import Pos, SokobanState
from sokobanlib.print import repr_solution


def solve(state: SokobanState) -> List[Solution]:
    return Solver().solve(state)


def calc_cost(p: Solution):
    pushes = len([m for m in p if m.is_push])
    all_moves = len(p)
    return (pushes, all_moves)


class Solver:
    solutions: Dict[SokobanState, Solution]

    def __init__(self):
        self.solutions = {}

    def solve(self, state: SokobanState) -> List[Solution]:
        all_states = self.generate(state)
        return sorted(
            (self.solutions[s] for s in all_states if s.is_solved()),
            key=calc_cost)

    def generate(self, state: SokobanState) -> List[SokobanState]:
        self.solutions[state] = []
        list(self.derive_states([state]))
        return list(self.solutions.keys())

    def derive_states(self,
                      states: List[SokobanState]) -> Iterable[SokobanState]:
        new_states: List[SokobanState] = []

        for state in states:
            if state.is_solved():
                continue

            path = self.solutions[state]

            for move in Move:
                new_state = self.move_or_push(state, move)
                if new_state is not None and new_state not in self.solutions:
                    self.solutions[new_state] = path + [move]
                    new_states.append(new_state)

        if not new_states:
            return []

        return self.derive_states(new_states)

    def simulate(self, state: SokobanState,
                 solution: Solution) -> SokobanState:
        for i, move in enumerate(solution):
            s = self.move_or_push(state, move)
            if s is not None:
                state = s
            else:
                raise Exception(
                    f"Impossible move: {repr_solution(solution[:i])}")
        return state

    def move_or_push(self, state: SokobanState,
                     move: Move) -> Optional[SokobanState]:
        if move.is_push and self.can_push(state, move):
            new_state = deepcopy(state)
            self.push(new_state, move)
            return new_state
        elif self.can_move(state, move):
            new_state = deepcopy(state)
            self.move(new_state, move)
            return new_state
        return None

    def move(self, state: SokobanState, direction: Move) -> None:
        state.pos = self.pos_after_move(state.pos, direction)

    def push(self, state: SokobanState, direction: Move) -> None:
        old_box_pos = self.pos_after_move(state.pos, direction)
        new_box_pos = self.pos_after_move(old_box_pos, direction)

        state[new_box_pos] = state[new_box_pos].add_box()
        state[old_box_pos] = state[old_box_pos].remove_box()

        self.move(state, direction)

    def can_move(self, state: SokobanState, direction: Move) -> bool:
        new_pos = self.pos_after_move(state.pos, direction)
        return state[new_pos].is_walkable

    def can_push(self, state: SokobanState, direction: Move) -> bool:
        new_pos = self.pos_after_move(state.pos, direction)
        if not state[new_pos].is_box:
            return False

        pos_behind_box = self.pos_after_move(new_pos, direction)
        return state[pos_behind_box].is_walkable

    def pos_after_move(self, pos: Pos, move: Move) -> Pos:
        return pos[0] + move.offset_x, pos[1] + move.offset_y
